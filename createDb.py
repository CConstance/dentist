from dentist import db, app
from dentist.models.video import Video
from dentist.models.user import User
from dentist.models.article import Article
from dentist.models.column import Column
from dentist.models.collect import Collect
from dentist.models.admin import Admin
with app.app_context():
    db.create_all()