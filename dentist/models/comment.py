#coding=utf-8
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import sessionmaker
from sqlalchemy import or_
from sqlalchemy.orm import *
from dentist import db
from dentist.models.video import Video

class Collect(db.Model):
    __tablename__ = 'comment'

    id       = db.Column(db.Integer, primary_key=True, autoincrement=True)
    vid      = db.Column(db.Integer)
    userId   = db.Column(db.Integer)
    comment  = db.Column(db.Text)
    created  = db.Column(db.DateTime)
    status   = db.Column(db.String(50))
    deleted  = db.Column(db.Boolean, default=False)

    def add(self):
        db.session.add(self)
        da.session.commit()

    def delete(self):
        self.deleted = True
        db.session.add(self)
        db.session.commit()

    @classmethod
    def get(cls,data):
        return cls.query.filter_by(vid = data).all()

    @classmethod
    def getAll(cls):
        return cls.query.filter(cls.deleted != 1)