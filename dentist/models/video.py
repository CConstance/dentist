#coding=utf-8
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import sessionmaker
from sqlalchemy import func, or_, not_, and_
from sqlalchemy import *
from sqlalchemy.orm import *
from dentist import db
# from sqlalchemy import func

class Video(db.Model):

    __tablename__ = 'video'

    id         = db.Column(db.Integer, primary_key=True, autoincrement=True)
    userId     = db.Column(db.Integer)
    description= db.Column(db.String(128))
    videoName  = db.Column(db.String(128))
    postUrl    = db.Column(db.String(128))
    videoUrl   = db.Column(db.String(128))
    tag        = db.Column(db.String(128))
    uploadTime = db.Column(db.DateTime)
    likeNumber = db.Column(db.Integer,default=0)
    status     = db.Column(db.Integer,default=0)
    deleted    = db.Column(db.Boolean, default=False)

    #获取一个视频信息
    @classmethod
    def get(cls,data):
        return cls.query.filter_by(id = data).first()

    #获取所有视频
    @classmethod
    def getAll(cls):
        return cls.query.filter(cls.deleted != 1).order_by(cls.status.desc(), cls.uploadTime.desc()).filter()
    #按条件读取视频列表
    @classmethod
    def search(cls,keyword):
       return cls.query.filter(
            or_(cls.videoName.like('%%%s%%' % keyword),
             cls.tag.like('%%%s%%' % keyword))).all()

    #新增一个视频
    def add(self):
        db.session.add(self)
        db.session.commit()

    #修改一个视频
    def update (self,data):
        self.videoName = data['videoName']
        self.description = data['description']
        self.tag = data['tag']
        db.session.add(self)
        db.session.commit()

    #删除一个视频
    def delete(self):
        self.deleted = 1
        db.session.add(self)
        db.session.commit()

    def statusUpdate(self,data):
        self.status = data
        db.session.add(self)
        db.session.commit()

    @classmethod
    def getTopFirst(cls,data):
        return cls.query.filter(
            and_(cls.status != 0, cls.tag == data, cls.deleted != 1)).order_by(cls.status.desc(), cls.uploadTime.desc()).filter()

    @classmethod
    def getByStatus(cls,data):
        return cls.query.filter(
            and_(cls.status == data,cls.deleted != 1)).order_by(cls.uploadTime.desc()).filter()
    
    @classmethod
    def count(cls):
        return cls.query.filter('').count()