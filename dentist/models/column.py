#coding=utf-8
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import sessionmaker
from sqlalchemy import or_, and_
from sqlalchemy.orm import *
from sqlalchemy import func
from dentist import db

class Column(db.Model):
    __tablename__ = 'column'

    id        = db.Column(db.Integer, primary_key=True, autoincrement=True)
    tag_name  = db.Column(db.String(128))
    childTag  = db.Column(db.Integer)
    order     = db.Column(db.Integer)
    # created   = db.Column(db.DateTime)
    type      = db.Column(db.Integer)
    status    = db.Column(db.Integer,default=0)
    createTime= db.Column(db.DateTime)
    deleted   = db.Column(db.Boolean, default=False)

    def add(self):
        print 'in'
        db.session.add(self)
        db.session.commit()
    
    def edit(self,data):
        key = self.query.get(self.id)
        key.tag_name = data['name']
        db.session.commit()

    def delete(self):
        self.deleted = True
        db.session.add(self)
        db.session.commit()

    @classmethod
    def getAll(cls):
        return cls.query.filter(cls.deleted != 1)

    @classmethod
    def getVideoColumn(cls):
        return cls.query.filter(and_(cls.type == 0, cls.deleted != 1, cls.status == 1))

    @classmethod
    def getByType(cls,data):
        return cls.query.filter(and_(cls.type == data, cls.deleted != 1))

    @classmethod
    def getByStatus(cls,type,status):
        return cls.query.filter(and_(cls.type == type, cls.status == status, cls.deleted != 1))

