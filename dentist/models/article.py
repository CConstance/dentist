#coding=utf-8
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import sessionmaker
from sqlalchemy import or_, and_, func
from sqlalchemy.orm import *
from dentist import db

class Article(db.Model):
    __tablename__ = 'article'

    id         = db.Column(db.Integer, primary_key=True, autoincrement=True)
    userId     = db.Column(db.Integer)
    title      = db.Column(db.String(128))
    description= db.Column(db.Text)
    content    = db.Column(db.Text)
    createTime = db.Column(db.DateTime)
    status     = db.Column(db.Integer,default=0)
    picture    = db.Column(db.String(128))
    poster     = db.Column(db.String(128))
    likeNumber = db.Column(db.Integer,default=0)
    tag        = db.Column(db.String(128))
    type       = db.Column(db.String(128))
    deleted    = db.Column(db.Boolean, default=False)
    
    def add(self):
        db.session.add(self)
        db.session.commit()

    def edit(self,data):
        key = self.query.get(self.id)
        key.content = date['content']
        key.picture = data['picture']
        db.session.commit()
        callback = self.query.get(self.id)
        return callback

    def delete(self):
        self.deleted = True
        db.session.add(self)
        db.session.commit()

    def statusUpdate(self,data):
        self.status = data
        db.session.add(self)
        db.session.commit()
    
        #修改一个视频
    def update (self,data):
        self.title       = data['title']
        self.description = data['description']
        self.tag         = data['tag']
        self.content     = data['content']
        db.session.add(self)
        db.session.commit()

    @classmethod
    def search(cls,keyword):
        return db.session.execute("SELECT article.id,article.userId,article.title,article.content,article.createTime,article.status, article.picture,article.likeNumber,article.deleted FROM user, article "
                                      "WHERE article.userId = user.id"
                                      " and (user.nickName LIKE '%" + keyword
                                      + "%' or article.content  LIKE '%'"
                                      + keyword +"%' or article.title LIKE '%'"
                                      + keyword +"%')").all()

    @classmethod
    def get(cls, data):
        return cls.query.filter_by(id = data).first()
      
    @classmethod
    def getAll(cls):
        return cls.query.filter(cls.deleted != 1).order_by(cls.status.desc(), cls.createTime.desc()).filter()

    @classmethod
    def getByUser(cls,userId):
        return cls.query.filter(cls.userId == userId, cls.deleted != 1).order_by(cls.status.desc(), cls.createTime.desc()).filter()


    @classmethod
    def getTopFirst(cls,data):
        return cls.query.filter(
            and_(cls.status != 0, cls.tag == data, cls.deleted != 1)).order_by(cls.status.desc(), cls.createTime.desc()).filter()

    @classmethod
    def getBytype(cls,data):
        return cls.query.filter(
            and_(cls.type == data, cls.deleted != 1)).order_by(cls.status.desc(), cls.createTime.desc()).filter()

    @classmethod
    def getTopAll(cls):
        return cls.query.filter(
            and_(cls.status != 0, cls.deleted != 1)).order_by(cls.status.desc(), cls.createTime.desc()).filter()

    @classmethod
    def count(cls):
        return cls.query.filter('').count()
    
    @classmethod
    def getByStatus(cls,data):
        return cls.query.filter(
            and_(cls.status == data,cls.deleted != 1)).order_by(cls.createTime.desc()).filter()
    

