#!/usr/bin/env python
#coding=utf-8
from sqlalchemy.ext.hybrid import hybrid_property
from dentist import db

class Admin(db.Model):
    id         = db.Column(db.Integer, primary_key=True, autoincrement=True)
    account    = db.Column(db.String(128))
    nickName   = db.Column(db.String(128))
    password   = db.Column(db.String(128))
    token      = db.Column(db.String(128))
    avatorUrl  = db.Column(db.String(128))
    createTime = db.Column(db.DateTime)
    status     = db.Column(db.String(128),default=1)
    deleted    = db.Column(db.Boolean, default=False)
    
    def __init__(self):
        pass
        
    @classmethod
    def get(cls,data):
        return cls.query.filter_by(id = data).first()

    def admin_login(self):
        callback = self.query.filter_by(
            account  = self.account,
            password = self.password,
            status   = '1').first()
        return callback

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def __repr__(self):
        return '<User %r>' % (self.nickName)