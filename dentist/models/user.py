#!/usr/bin/env python
#coding=utf-8
from sqlalchemy.ext.hybrid import hybrid_property
from dentist import db

class User(db.Model):
    id         = db.Column(db.Integer, primary_key=True, autoincrement=True)
    roles      = db.Column(db.String(128),default=0)
    account    = db.Column(db.String(128))
    name       = db.Column(db.String(128))
    nickName   = db.Column(db.String(128))
    password   = db.Column(db.String(128))
    telephone  = db.Column(db.String(128))
    address    = db.Column(db.String(128))
    docId      = db.Column(db.String(128))
    token      = db.Column(db.String(128))
    avatorUrl  = db.Column(db.String(128))
    description= db.Column(db.String(128))
    createTime = db.Column(db.DateTime)
    deleted    = db.Column(db.Boolean, default=False)
    
    def __init__(self):
        pass

    #用户登录
    def login(self):
        callback = self.query.filter_by(
            account = self.account,
            password = self.password).first()
        return callback

    def getByAccount(cls,data):
        return cls.query.filter_by(account = data).first()

    #用户注册
    def add(self):
        db.session.add(self)
        db.session.commit()

    def upgrade(self,data):
        key = self.query.get(self.id)
        key.roles = data
        # db.session.add(self)
        db.session.commit()

    #修改用户信息
    def update(self,data):
        key = self.query.get(self.id)
        key.nickName = data['nickName']
        key.name     = data['name']
        key.nickName = data['nickName']
        key.telephone= data['telephone']
        key.address  = data['address']
        key.docId    = data['docId']
        key.description = data['description']
        db.session.commit()
        callback   = self.query.get(self.id)
        return callback
    
    #删除用户
    def delete(self):
        self.deleted = True
        db.session.add(self)
        db.session.commit()

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def __repr__(self):
        return '<User %r>' % (self.nickName)

    @classmethod
    def count(cls):
        return cls.query.filter('').count()

    #获取单个用户详情
    @classmethod
    def get(cls,data):
        return cls.query.filter_by(id = data).first()

    #或取全部用户详情（管理员）
    @classmethod
    def getAll(cls):
        return cls.query.filter(cls.deleted != 1).order_by(cls.roles.desc(), cls.createTime.desc()).filter()
