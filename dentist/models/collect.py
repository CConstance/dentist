#coding=utf-8
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import sessionmaker
from sqlalchemy import or_,and_
from sqlalchemy.orm import *
from dentist import db
from dentist.models.article import Article
from dentist.models.video import Video

class Collect(db.Model):
    __tablename__ = 'collect'

    id        = db.Column(db.Integer, primary_key=True, autoincrement=True)
    type      = db.Column(db.Integer)
    collectId = db.Column(db.Integer)
    userId    = db.Column(db.Integer)
    created   = db.Column(db.DateTime)
    deleted   = db.Column(db.Boolean, default=False)

    def add(self):
        db.session.add(self)
        da.session.commit()

    def cancel(self):
        self.deleted = True
        db.session.add(self)
        db.session.commit()

    @classmethod
    def getVideo(cls,data):
        return cls.query.filter_by(and_(userId = data, type = 0)).all()

    @classmethod
    def getArticle(cls,data):
        return cls.query.filter_by(and_(userId = data, type = 1)).all()

    @classmethod
    def getAll(cls):
        return cls.query.filter(cls.deleted != 1)