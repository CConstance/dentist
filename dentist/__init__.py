#coding=utf-8
from flask import Flask
from flask.ext.assets import Environment, Bundle
from flask.ext.sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CsrfProtect
from flask.ext.login import LoginManager 
from flask.ext.principal import Principal, Permission, RoleNeed

app           = Flask(__name__)
db            = SQLAlchemy(app)
principals    = Principal(app)
login_manager = LoginManager()
login_manager.login_view = "router.router_index"
# CsrfProtect(app)
login_manager.setup_app(app)
admin_permission = Permission(RoleNeed('admin'))
vip_permission = Permission(RoleNeed('vip'))
app.config.from_object('config.default')
app.config['SQLALCHEMY_DATABASE_URI']

db = SQLAlchemy(app)

from views.video import video
from views.user import user
from views.article import article
from views.column import column
from views.router import router
from util.status import status

app.register_blueprint(video) 
app.register_blueprint(user) 
app.register_blueprint(article)
app.register_blueprint(router)
app.register_blueprint(column)
app.register_blueprint(status)
#from util.assets import assets
