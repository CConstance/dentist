from flask import Blueprint, render_template, redirect, url_for, request, jsonify, json
from dentist.models.video import Video
from dentist.models.article import Article

status = Blueprint('status', __name__, url_prefix='/status')
@status.route('/update/<int:type>/<int:Id>/<int:data>', methods=["POST"])
def update(type,Id,data):
    if type == 0:
        video =  Video.query.get_or_404(Id)
        video.statusUpdate(data)
        return jsonify(success=True) 
    if type == 1:
        article =  Article.query.get_or_404(Id)
        article.statusUpdate(data)
        return jsonify(success=True)
   