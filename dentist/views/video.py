from flask import Blueprint, render_template, redirect, url_for, request, jsonify, json
from dentist.models.video import Video
from dentist.models.column import Column
from dentist.models.user import User
#from dentist.forms import sign, signUp, editForm
from wtforms.validators import ValidationError
from werkzeug import secure_filename
from flask.ext.login import login_user, logout_user, current_user, login_required
import datetime
# from .models.video import Video
video = Blueprint('video', __name__, url_prefix='/video')

@video.route('/get/<int:videoId>', methods=["POST"])
@login_required
def video_get(videoId):
    video    = Video.query.get_or_404(videoId)
    return jsonify(userId = video.userId, description = video.description, videoName = video.videoName, videoUrl = video.videoUrl, posterUrl = video.postUrl,tag = video.tag)

@video.route('/play/<int:videoId>', methods=["GET"])
def video_play(videoId):
    # video    = Video.query.get_or_404(videoId)
    # if video.deleted == True:
    #     return jsonify(success=False)
    return render_template('video/play.html',videoId = videoId)

@video.route('/adminplay/<int:videoId>', methods=["GET"])
def video_view(videoId):
    video    = Video.query.get_or_404(videoId)
    if video.deleted == True:
        return jsonify(success=False)
    return render_template('admin/viewplay.html',userId = video.userId, description = video.description, videoName = video.videoName, videoUrl = video.videoUrl, posterUrl = video.postUrl,tag = video.tag)

@video.route('/getall', methods=["POST"])
def video_getAll():
    data      = request.get_json()
    page      = data['pages']
    per_page  = 10
    totalPage = Video.count() / per_page
    paginate  = Video.getAll().paginate(page, per_page)
    videos    = paginate.items
    return jsonify(total = totalPage,videos = [{
        'id':each.id,
        'userId': each.userId,
        'videoName': each.videoName,
        'videoUrl': each.videoUrl,
        'postUrl': each.postUrl,
        'uploadTime':each.uploadTime,
        'tag':  Column.query.get_or_404(each.tag).tag_name,
        'status'    : each.status,
    } for each in videos])

@video.route('/search',methods=["POST"])
def video_search():
    keyword = request.form.get('keyword', None)
    if keyword is None:
        return jsonify(success=False)
    videos = Video.search(keyword)
    return jsonify(videos=[{
        'user_id': video.user_id,
        'name': video.name,
        'url': video.url,
        'tag': video.tag,
    } for video in videos])

#@video.route('/videoadd', methods=["POST","GET"])
#def videoAdd():
 
@video.route('/update/<int:videoId>',methods=["POST"])
def video_update(videoId):
    data = request.get_json()
    video = Video.query.get_or_404(videoId)
    video.update(data)
    return jsonify(success=True)
    # callback = video.query.get(video.id)
   # if (form.videoName.data != '' || form.tag.data != ''):
    #    if (form.validate_on_submit()):
     #       callback = video.videoUpdate(form.data)

    # return jsonify(
    #     userId    = callback.userId,        
    #     videoName = callback.videoName,
    #     videoUrl  = callback.videoUrl,
    #     tag       = callback.tag,)

@video.route('/delete/<int:videoId>', methods=["POST"])
def video_delete(videoId):
    video = Video.query.get_or_404(videoId)
    video.delete()
    return jsonify(success=True)

@video.route('/uploadvideo', methods=["POST"])
def video_uploadFile():
    # time     = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    files    = request.files['files']
    filename = secure_filename(files.filename)
    files.save('/dentist/dentist/static/uploads/video/'+filename)
    return jsonify(success=True)

@video.route('/uploadposter', methods=["POST"])
def video_uploadPoster():
    # time     = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    files    = request.files['files']
    filename = secure_filename(files.filename)
    files.save('/dentist/dentist/static/uploads/video/poster/'+filename)
    return jsonify(success=True)

@video.route('/add', methods=["POST"])
def video_add():
    data     = request.get_json()
    video    = Video()
    # video.userId = 
    video.videoName   = data['videoName']
    video.videoUrl    = data['videoUrl']
    video.postUrl     = data['postUrl']
    video.description = data['description']
    video.tag         = data['tag']
    video.uploadTime  = datetime.datetime.now()
    video.add()
    return jsonify(success=True)

@video.route('/list/<int:number>/<int:tag>', methods=["POST"])
def list(number,tag):
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', number, type=int)
    paginate = Video.getTopFirst(tag).paginate(page, per_page)
    videos = paginate.items
    return jsonify(videos = [{
        'id'       : each.id,
        'userId'   : each.userId,
        'postUrl'  : each.postUrl,
        'description':each.description,
        'videoName': each.videoName,
        'videoUrl' : each.videoUrl,
        'tag'      : Column.query.get_or_404(each.tag).tag_name,
        'likeNumber': each.likeNumber,
        'uploadTime': each.uploadTime,
    } for each in videos])

@video.route('/listall/<int:number>', methods=["GET"])
def videolist(number):
    taglist = Column.getVideoColumn()
    total   = taglist.count()
    result  = []
    for i in range(0,total):  
        videos = Video.getTopFirst(taglist[i].id)
        if (videos.count() == 0):
            break;
      #  result.append(taglist[i].tag_name)   
       # print(taglist[i].id)
        if(number <= videos.count()):
            for x in range(0,number):
                result.append({
                'tagName': taglist[i].tag_name,
                'videoName': videos[x].videoName,
                'videoUrl': videos[x].videoUrl,
                'tag': videos[x].tag,
                'created':videos[x].uploadTime,
                'videoId':videos[x].id,
                'postURL':videos[x].videoUrl
                })
        else:
            for x in range(0,videos.count()):
                result.append({
                'tagName': taglist[i].tag_name,
                'videoName': videos[x].videoName,
                'videoUrl': videos[x].videoUrl,
                'tag': videos[x].tag,
                'created':videos[x].uploadTime,
                'videoId':videos[x].id,
                'postURL':videos[x].videoUrl
                })

    return jsonify(results = [{
        'tagname': each['tagName'],
        'videoname': each['videoName'],
        'videoUrl': each['videoUrl'],
    } for each in result])
      
@video.route('/show/<int:number>/<int:status>', methods=["POST"])
def public_list(number,status):
    page     = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', number, type=int)
    paginate = Video.getByStatus(status).paginate(page, per_page)
    videos   = paginate.items
    return jsonify(videos = [{
        # 'userName'  : User.query.get_or_404(each.userId).nickName,
        'id'        : each.id,
        'videoName' : each.videoName,
        'tag'       : Column.query.get_or_404(each.tag).tag_name,
        'uploadTime': each.uploadTime,
        'status'    : each.status,
    } for each in videos])

