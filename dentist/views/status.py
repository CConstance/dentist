from flask import Blueprint, render_template, redirect, url_for, request, jsonify, json
from dentist.models.video import Video
from dentist.models.article import Article
# from dentist import app

status = Blueprint('status', __name__, url_prefix='/status')
# @video.route('/update/<int:type>/<int:Id>/<int:status>', methods=["POST"])
@status.route('/update/<int:type>/<int:Id>/<int:status>', methods=["POST"])
def update(type,Id,status):
    if  (type == 0):
        video =  Video.query.get_or_404(Id)
        video.statusUpdate(status)
        return jsonify(success=True) 
    if (type == 1):
        article =  Article.query.get_or_404(Id)
        article.statusUpdate(status)
        return jsonify(success=True)

