from flask import Blueprint, render_template, redirect, url_for, request, jsonify, json
from flask.ext.login import login_user, logout_user, current_user, login_required
from dentist import login_manager,admin_permission,vip_permission,app
from dentist.models.user import User
from flask.ext.principal import identity_loaded, RoleNeed, UserNeed,PermissionDenied

router = Blueprint('router', __name__, url_prefix='')

@router.route('/',methods=['GET'])
def router_index():
    return render_template('home/index.html')

@router.route('/video',methods=['GET'])
def router_video():
    return render_template('video/list.html')

@router.route('/article',methods=['GET'])
def router_articleList():
    return render_template('article/list.html',type = '1')

@router.route('/articles',methods=['GET'])
def router_articlesList():
    return render_template('article/list.html',type = '2')

@router.route('/kqmoocadmin/signin', methods=["POST"])
def admin_login():
    data          = request.get_json()
    user          = User()
    user.account  = data['account']
    user.password = data['password']
    callback      = user.login()
    if(callback != None):
        login_user(callback)
        return jsonify(
            statusCode  = "200",
            accountId   = callback.id,
            account     = callback.account,
            nickName    = callback.nickName,)
    else :
        return jsonify(statusCode="201")

@router.route('/kqmoocadmin/login',methods=['GET'])
def router_admin_login():
    return render_template('admin/login.html',statusCode = 200)

@router.route('/kqmoocadmin/login/error',methods=['GET'])
def router_admin_loginRedirect():
    return render_template('admin/login.html',statusCode = 500)

@router.route('/kqmoocadmin',methods=['GET'])
@admin_permission.require()
def router_admin():
    return render_template('admin/home.html')

@router.route('/kqmoocadmin/public',methods=['GET'])
@admin_permission.require()
def router_admin_public():
    return render_template('admin/public.html')

@router.route('/kqmoocadmin/video',methods=['GET'])
@admin_permission.require()
def router_admin_video():
    return render_template('admin/video.html')

@router.route('/kqmoocadmin/videoadd',methods=['GET'])
@admin_permission.require()
def router_admin_videoAdd():
    return render_template('admin/videoadd.html')

@router.route('/kqmoocadmin/videoedit/<int:videoId>',methods=['GET'])
@admin_permission.require()
def router_admin_videoEdit(videoId):
    return render_template('admin/videoedit.html',id = videoId)

@router.route('/kqmoocadmin/article',methods=['GET'])
@admin_permission.require()
def router_admin_article():
    return render_template('admin/article.html')

@router.route('/kqmoocadmin/articleadd',methods=['GET'])
@admin_permission.require()
def router_admin_articleadd():
    return render_template('admin/articleadd.html')

@router.route('/kqmoocadmin/editarticle/<int:articleId>',methods=['GET'])
@admin_permission.require()
def router_admin_articleEdit(articleId):
    return render_template('admin/editarticle.html',id = articleId)

@router.route('/kqmoocadmin/adv',methods=['GET'])
@admin_permission.require()
def router_admin_adv():
    return render_template('admin/adv.html')

@router.route('/kqmoocadmin/user',methods=['GET'])
@admin_permission.require()
def router_admin_user():
    return render_template('admin/user.html')

@router.route('/kqmoocadmin/class',methods=['GET'])
@admin_permission.require()
def router_admin_class():
    return render_template('admin/class.html')

@app.errorhandler(PermissionDenied)
def permissionDenied(error):
    return render_template('tpl/404.html')

@router.route('/usercenter/view/<int:userId>',methods=['GET'])
@login_required
def userCenter_view(userId):
    return render_template('usercenter/view.html',userId=userId)

@router.route('/usercenter',methods=['GET'])
@login_required
def userCenter_index():
    return render_template('usercenter/index.html')

@router.route('/usercenter/collect',methods=['GET'])
@login_required
def userCenter_collect():
    return render_template('usercenter/collect.html')

@router.route('/usercenter/public',methods=['GET'])
@login_required
def userCenter_public():
    return render_template('usercenter/publish.html')

@router.route('/usercenter/add',methods=['GET'])
@vip_permission.require()
def userCenter_add():
    return render_template('usercenter/add.html')

@router.route('/kqmoocadmin/author',methods=['GET'])
@admin_permission.require()
def author():
    return render_template('admin/author.html')

@router.route('/kqmoocadmin/authoradd',methods=['GET'])
@admin_permission.require()
def author_add():
    return render_template('admin/authoradd.html')