from flask import Blueprint, render_template, redirect, url_for, request, jsonify, json
from dentist.models.column import Column
#from dentist.forms import sign, signUp, editForm
from wtforms.validators import ValidationError


column = Blueprint('column', __name__, url_prefix='/column')

@column.route('/delete/<int:columnId>',methods=["POST"])
def column_delete(columnId):
    column    = Column.query.get_or_404(columnId)
    column.delete()
    return jsonify(success=True)

@column.route('/getall',methods=["POST"])
def column_getAll():
    # page = request.args.get('page', 1, type=int)
    # per_page = request.args.get('per_page', 10, type=int)
    # paginate = Column.getAll().paginate(page, per_page)
    # columns = paginate.items
    columns = Column.getAll()
    return jsonify(columns= [{
        'id'   : each.id,
        'tag'  : each.tag_name,
        'type' : each.type,
        'status': each.status,
        # 'count': eaach.count(each.id)
    } for each in columns])

@column.route('/add',methods=["POST"])
def column_add():
    data            = request.get_json()
    column          = Column()
    column.tag_name = data['className']
    column.type     = data['type']
    column.add() 
    return jsonify(success=True)

@column.route('/updateStatus/<int:Id>/<int:status>', methods=["POST"])
def updateStatus(Id,status):
    column =  Column.query.get_or_404(Id)
    column.statusUpdate(status)
    return jsonify(success=True)

#getByType
@column.route('/getColumnByType/<int:type>',methods=["POST"])
def column_getByType(type):
    columns = Column.getByType(type)
    return jsonify(columns= [{
        'id'   : each.id,
        'tag'  : each.tag_name,
        'type' : each.type,
        'status': each.status,
        # 'count': eaach.count(each.id)
    } for each in columns])

#getByTypeWStatus
@column.route('/getColumnByStatus/<int:type>/<int:status>',methods=["POST"])
def column_getByTypeWStatus(type,status):
    columns = Column.getByStatus(type,status)
    return jsonify(columns= [{
        'id'   : each.id,
        'tag'  : each.tag_name,
        'type' : each.type,
        'status': each.status,
        # 'count': eaach.count(each.id)
    } for each in columns])