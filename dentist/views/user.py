#!/usr/bin/env python
#coding=utf-8
from flask import Blueprint, render_template, redirect, url_for, request, jsonify, json, session
from dentist.models.user import User
from dentist.models.admin import Admin
from dentist import login_manager
from flask.ext.login import login_user, logout_user, current_user, login_required
from flask.ext.principal import identity_loaded, RoleNeed, UserNeed,identity_changed, current_app,Identity, AnonymousIdentity
import datetime
from dentist import app
user = Blueprint('user', __name__, url_prefix='/user')

@user.route('/login', methods=["POST"])
def login():
    data          = request.get_json()
    user          = User()
    user.account  = data['account']
    user.password = data['password']
    callback      = user.login()
    if(callback != None):
        login_user(callback)
        identity_changed.send(current_app._get_current_object(),identity=Identity(callback.id))
        return jsonify(
            statusCode  = "200",
            role        = callback.roles,
            accountId   = callback.id,
            account     = callback.account,
            nickName    = callback.nickName,)
    else :
        return jsonify(statusCode="201")

@user.route('/logout',methods=['GET'])
def logout():
    logout_user()
    for key in ('identity','identity.name', 'identity.auth_type'):
        session.pop(key, None)

    # identity_changed.send(current_app._get_current_object(),identity=AnonymousIdentity())
    return redirect(url_for('router.router_index'))

@user.route('/upgrade/<int:role>',methods=['POST'])
def upgrade(role):
    user = User()
    user.id = current_user.id
    user.upgrade(role)
    return jsonify(statusCode = "200")

@user.route('/regist', methods=["POST"])
def regist():   
    data          = request.get_json()
    user          = User()
    user.account  = data['account']
    user.password = data['password']
    user.createTime = datetime.datetime.now()
    if user.getByAccount(user.account):
        return jsonify(statusCode = 201)
    else :
        user.nickName = user.account
        user.add()
        callback      = user.getByAccount(data['account'])
        login_user(callback)
        return jsonify(statusCode = "200")

@user.route('/get/<int:userId>', methods=["POST"])
def user_get(userId):
    user = User.query.get_or_404(userId)
    if user.deleted == True:
        return jsonify(success=False)
    return jsonify(account=user.account,
                   name=user.name,
                   telephone=user.telephone,
                   address=user.address,
                   docId=user.docId,
                   token=user.token,
                   avatorUrl=user.avatorUrl,
                   description=user.description,
                   nickName=user.nickName, 
                   deleted=user.deleted,)

# @user.route('/list', methods=["POST"])
# @login_required
# def user_getAll():
#     page = request.args.get('page', 1, type=int)
#     per_page = request.args.get('per_page', 20, type=int)
#     paginate = User.getAll().paginate(page, per_page,False)
#     users = paginate.items
#     return jsonify(users=[{
#         'account': each.account,
#         'nickname': each.nickName,
#     } for each in users])

@user.route('/list', methods=["POST"])
def user_getAll():
    data      = request.get_json()
    page      = data['pages']
    per_page  = 10
    totalPage = User.count() / per_page
    paginate  = User.getAll().paginate(page, per_page,False)
    users = paginate.items
    return jsonify(total = totalPage,users=[{
        'id'      : each.id,
        'account' : each.account,
        'nickName': each.nickName,
        'docId'   : each.docId,
        'createTime':each.createTime,
        'roles'   : each.roles
    } for each in users])

@user.route('/delete/<int:userId>',methods=["POST"])
@login_required
def user_delete(userId):
    user = User.query.get_or_404(userId)
    user.delete()
    return jsonify(success=True)

@user.route('/update',methods=['POST'])
def update():
    data = request.get_json()
    user = User()
    user.id = int(data['id'])
    # print current_user
    user.update(data)
    return jsonify(statusCode="200")
    # user.avatorUrl = data['avatorUrl']


@login_manager.user_loader
def load_user(userid):
    return User.get(userid)

@identity_loaded.connect_via(app)
def on_identity_loaded(sender, identity):
    # Set the identity user object
    identity.user = current_user
    #先给登录用户赋予通用权限
    identity.provides.add(RoleNeed('loginUser'))
    if hasattr(identity.user, 'roles'):
        if identity.user.roles == '1':
            identity.provides.add(RoleNeed('admin'))
        else :
            if identity.user.roles == '2':
                identity.provides.add(RoleNeed('vip'))
    else:
        pass
    # Add the UserNeed to the identity
    # if hasattr(current_user, 'id'):
    #     identity.provides.add(UserNeed(current_user.id))

    # # Assuming the User model has a list of roles, update the
    # # identity with the roles that the user provides
    # if hasattr(current_user, 'roles'):
    #     identity.provides.add(RoleNeed(current_user.roles))