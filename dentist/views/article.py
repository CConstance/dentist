from flask import Blueprint, render_template, redirect, url_for, request, jsonify, json
from dentist.models.article import Article
from dentist.models.column import Column
from dentist.models.user import User
#from dentist.forms import sign, signUp, editForm
from wtforms.validators import ValidationError
from werkzeug import secure_filename
from sqlalchemy.orm import *
import datetime
from flask.ext.login import current_user
article = Blueprint('article', __name__, url_prefix='/article')

@article.route('/get/<int:articleId>', methods=["GET","POST"])
def article_get(articleId):
    article    = Article.query.get_or_404(articleId)
    return jsonify(
        userId = article.userId, 
        title = article.title,
        createTime = article.createTime, 
        description = article.description, 
        picture = article.picture,
        tag = Column.query.get_or_404(article.tag).tag_name,
        likeNumber =article.likeNumber,
        content = article.content,
        type    = article.type)

@article.route('/<int:articleId>',methods=['GET'])
def router_article(articleId):
    # article    = Article.query.get_or_404(articleId)
    return render_template('article/article.html',articleId = articleId)

@article.route('/delete/<int:articleId>',methods=["POST"])
def artice_delete(articleId):
    article     = Article.query.get_or_404(articleId)
    article.delete()
    return jsonify(success=True)

@article.route('/search',methods=['POST'])
def artice_search():
    keyword = request.form.get('keyword',None)
    if keyword is None:
      return jsonify(success=False)
    articles = Article.search(keyword)
    return jsonify(articles=[article.title for article in articles])

@article.route('/get/<int:articleId>',methods=["GET"])
def artice_get(articleId):
    #article = User.query.get_or_404(articleId)
    article = Article.get(articleId)
    if article.deleted == True:
      return jsonify(success=False)
    return jsonify(
          title = article.title,)

@article.route('/userget/<int:userId>',methods=["POST"])
def artice_getByUser(userId):
    articles = Article.getByUser(userId)
    return jsonify(articles = [{
        'id':each.id,
        'userId': each.userId,
        'title': each.title,
        'description':each.description,
        'poster':each.poster,
        'createTime':each.createTime,
        'tag':  Column.query.get_or_404(each.tag).tag_name,
        'status'    : each.status,
    } for each in articles])

@article.route('/getall',methods=["POST"])
def artice_getAll():
    articles = Article.getAll()
    return jsonify(articles = [{
        'id':each.id,
        'userId': each.userId,
        'title': each.title,
        'description':each.description,
        'poster':each.poster,
        'createTime':each.createTime,
        'tag':  Column.query.get_or_404(each.tag).tag_name,
        'status'    : each.status,
    } for each in articles])


@article.route('/list/<int:number>/<int:tag>', methods=["POST"])
def list(number,tag):
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', number, type=int)
    paginate = Article.getTopFirst(tag).paginate(page, per_page)
    articles = paginate.items
    return jsonify(articles = [{
        'userId': each.userId,
        'title': each.title,
        'tag': each.tag,
    } for each in articles])


@article.route('/listall/<int:type>', methods=["POST"])
def listAll(type):
    data      = request.get_json()
    page      = data['pages']
    per_page  = 10
    totalPage = Article.count() / per_page
    paginate  = Article.getBytype(type).paginate(page, per_page)
    articles = paginate.items
    return jsonify(articles = [{
        'id':each.id,
        'userId': User.get(each.userId).nickName,
        'title': each.title,
        'description':each.description,
        'poster':each.poster,
        'createTime':each.createTime,
        'tag':  Column.query.get_or_404(each.tag).tag_name,
        'status'    : each.status,
    } for each in articles],totalPage = totalPage)

@article.route('/listlimit/<int:number>', methods=["POST"])
def listLimit(number):
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', number, type=int)
    paginate = Article.getTopAll().paginate(page, per_page)
    articles = paginate.items
    return jsonify(articles = [{
        'userId': each.userId,
        'title': each.title,
        'tag': each.tag,
    } for each in articles])

@article.route('/uploadposter', methods=["POST"])
def article_uploadPoster():
    # time     = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    files    = request.files['files']
    filename = secure_filename(files.filename)
    files.save('/dentist/dentist/static/uploads/article/poster/'+filename)
    return jsonify(success=True)

@article.route('/uploadpic', methods=["POST"])
def article_uploadPic():
    # time     = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    files    = request.files['files']
    filename = secure_filename(files.filename)
    files.save('/dentist/dentist/static/uploads/article/picture/'+filename)
    return jsonify(success=True)

@article.route('/add', methods=["POST"])
def article_add():
    data                = request.get_json()
    article             = Article()
    article.userId      = current_user.id
    article.title       = data['title']
    article.description = data['description']
    article.tag         = data['tag']
    article.type        = data['type']
    article.content     = data['content']
    article.picture     = data['picUrl']
    article.poster      = data['postUrl']
    article.createTime  = datetime.datetime.now()
    article.add()
    return jsonify(success=True)

@article.route('/update/<int:articleId>',methods=["POST"])
def article_update(articleId):
    data = request.get_json()
    article = Article.query.get_or_404(articleId)
    article.update(data)
    return jsonify(success=True)

@article.route('/show/<int:number>/<int:status>', methods=["POST"])
def article_list(number,status):
    page     = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', number, type=int)
    paginate = Article.getByStatus(status).paginate(page, per_page)
    articles = paginate.items
    return jsonify(articles = [{
        # 'userName'  : User.query.get_or_404(each.userId).nickName,
        'id'        : each.id,
        'userId'    : each.userId,
        'title'     : each.title,
        'tag'       : Column.query.get_or_404(each.tag).tag_name,
        'createTime': each.createTime,
        'status'    : each.status,
        'picture'   : each.picture,
    } for each in articles])

