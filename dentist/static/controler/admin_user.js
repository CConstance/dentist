$(function(){

    getContent(1);
    function getContent(pages){
        // console.log(pages)
        getAjax('/user/list',{"pages":pages},function(callback){
            console.log(callback)
            var length     = callback.users.length,
                data       = callback.users,
                userStatus = '', 
                html       = '',
                pageHtml   = '<span class="disabled prev">«</span>',
                totalPage  = callback.total,
                run        = 0;
            if(!length){
                html = '<tr><td colspan="6">'+table_text_null+'</td></tr>';
                $('tbody').html(html);
                return 
            }
            for(var j = 1 ;j<=totalPage+1;j++){
                if(pages == j)
                    pageHtml += '<span class="current " id="page-'+j+'">'+j+'</span>'
                else
                    pageHtml += '<span class="default" id="page-'+j+'">'+j+'</span>'
            }
            for(var i = 0; i<length;i++){
                userStatus = getUserStatus(data[i].roles)
                html +='<tr id="'+data[i].id+'">'
                +'<th scope="row">'+(i+1)+'</th>'
                +'<td><a href="/video/adminplay/'+data[i].id+'">'+data[i].nickName+'</a></td>'
                +'<td>'+data[i].account+'</td>'
                +'<td>'+userStatus+'</td>'
                +'<td>'+data[i].docId+'</td>'
                +'<td>'+FormatDate(data[i].createTime,0)+'</td>'
                +'<td data-role='+data[i].roles+'>'
                // +    '<button type="button" class="btn btn-default btn-xs" data-action="top" data-toggle="button" aria-pressed="false" autocomplete="off"><span class="glyphicon glyphicon-star"> </span>  置顶 </button>'
                +    '<button type="button" class="btn btn-default btn-xs" data-action="view" ><span class="glyphicon glyphicon-eye-open"> </span> 查看 </button></a>';
                for(run;run<3;run++){
                    if(run == data[i].roles)
                        html += '<button type="button" class="btn btn-default btn-xs disabled" data-action="update" data-id="'+run+'">'+getUserStatus(run)+' </button>'
                    else
                        html +='<button type="button" class="btn btn-default btn-xs " data-action="update" data-id="'+run+'">'+getUserStatus(run)+' </button>'

                }
                html += 
                '<button type="button" class="btn btn-default btn-xs" data-action="delete"><span class="glyphicon glyphicon-remove"> </span> 删除 </button>'
                +'</td>'
              +'</tr>'
            }
            pageHtml += '<span class="disabled next" >»</span>';
            $('tbody').html(html);
            $('.qzPagination').html(pageHtml)
            $('button').click(function() {
                var action = $(this).data('action'),
                    id= $(this).parents('tr')[0].id,
                    roleOld = '',
                    roleCur = '';
                switch(action){
                    case 'view':
                        // window.location.href = '/video/adminplay/'+publicId;
                        break;
                    case 'delete':
                        $(this).parents('tr').hide();
                        getAjax('/user/delete/'+id,'',function () {
                            $(this).parents('tr').hide();
                        })
                        break;
                    case 'update':
                        roleOld = $(this).parent().data('role');
                        roleCur = $(this).data('id')
                        if(roleCur!=roleOld){
                            getAjax('/user/upgrade/'+roleCur,'',function () {
                            // $(this).parents('tr').hide();

                            })
                        }
                        
                        break;
                }
            });
            $('.qzPagination span').click(function(){
                var spanId = this.id,
                    pageId = Number(spanId.substr(5,spanId.length));
                getContent(pageId)
            })
            // window.location.href = callback.url 
        })
    } 
})