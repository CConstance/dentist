$(function(){
    getAjax('/column/getColumnByType/'+type,'',function(callback){
        var length = callback.columns.length,
            data   = callback.columns,
            html   = '';
        for(var i = 0;i<length;i++){
            html += '<option id="'+data[i].id+'">'+data[i].tag+'</option>'
        }
        $('.classify').html(html);
    })
    $('#content').wangEditor();
    $("#file-1").fileinput({
        uploadUrl: '/article/uploadposter', 
        allowedFileExtensions : ['jpg','png','jpeg','gif'],
        overwriteInitial: false,
        // maxFileSize: 1000,
        // maxFilesNum: 10,
        allowedFileTypes: ['image'],
        slugCallback: function(filename) {
          console.log('ok')
          $('#postUrl').val(filename)
          return filename.replace('(', '_').replace(']', '_');
      }
    })
    $("#file-2").fileinput({
        uploadUrl: '/article/uploadpic', 
        allowedFileExtensions : ['jpg','png','jpeg','gif'],
        overwriteInitial: false,
        // maxFileSize: 1000,
        // maxFilesNum: 10,
        allowedFileTypes: ['image'],
        slugCallback: function(filename) {
          console.log('ok')
          $('#picUrl').val(filename)
          return filename.replace('(', '_').replace(']', '_');
      }
    })
    $('.commitBtn').click(function () {
        var title       = $('#title').val(),
        description = $('#describe').val(),
        tag         = $("select option:checked").attr("id"),
        content     = $('#content').val(),
        postUrl     = $('#_poster .file-caption-name').html();
        picUrl      = $('#_pic .file-caption-name').html();
        getAjax('/article/add',{"title":title,"content":content,"description":description,"postUrl":postUrl,"tag":tag,"picUrl":picUrl,"type":type},function(){
            window.location.href = '/kqmoocadmin/article'
                // window.location.href = callback.url 
            })

    })
});