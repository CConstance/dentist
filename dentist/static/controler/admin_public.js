$(function(){
    getAjax('/video/show/10/0','',function(callback){
        // console.log(callback)
        var videoHtml ='',
            data = callback.videos,
            length    = data.length;
        if(!length){
            videoHtml = '<tr><td colspan="7">'+table_text_null+'</td></tr>';
            $('#collapseOne tbody').html(videoHtml);
            return 
        }
        for(var i = 0;i< data.length;i++){
            videoHtml += '<tr id="'+data[i].id+'">'
                +'<td scope="row">'+(i+1)+'</td>'
                +'<td><a href="">'+data[i].videoName+'</a></td>'
                +'<td>'+data[i].tag+'</td>'
                +'<td>管理员</td>'
                +'<td >'+FormatDate(data[i].uploadTime,0)+'</td>'
                +'<td>'
                +    '<div class="btn-group publicControler1" data-toggle="buttons">'
                +      '<label class="btn btn-danger btn-xs publicOn">'
                +        '<input type="radio" name="options" autocomplete="off" > &nbsp;&nbsp;&nbsp;发布中&nbsp;&nbsp;&nbsp;'
                +      '</label>'
                +      '<label class="btn btn-danger btn-xs active publicOff">'
                +        '<input type="radio" name="options" autocomplete="off" > &nbsp;&nbsp;&nbsp;下架中&nbsp;&nbsp;&nbsp;'
                +      '</label>'
                +    '</div>'
                +'</td>'
                +'<td>'
                +    '<button type="button" class="btn1 btn btn-default btn-xs" data-action="top" data-toggle="button" aria-pressed="false" autocomplete="off"><span class="glyphicon glyphicon-star"> </span>  置顶 </button>'
                +    '<button type="button" class="btn1 btn btn-default btn-xs" data-action="view"><span class="glyphicon glyphicon-eye-open"> </span> 查看 </button>'
                +    '<button type="button" class="btn1 btn btn-default btn-xs" data-action="edit"><span class="glyphicon glyphicon-edit"> </span> 编辑 </button>'
                +    '<button type="button" class="btn1 btn btn-default btn-xs" data-action="delete"><span class="glyphicon glyphicon-remove"> </span> 删除 </button>'
                +'</td>'
              +'</tr>'
        }
        $('#collapseOne tbody').html(videoHtml);
        $('.publicControler1 label').click(function(){
            var publicId = $(this).parents('tr')[0].id;
            if($(this).hasClass('publicOn'))
                changePublicStatus(publicId,0,1);
            else if($(this).hasClass('publicOff'))
                changePublicStatus(publicId,0,0);
            // $(this).parents('tr').hide()
        })
        $('.btn1').click(function(){
            var control = $(this).data('action'),
                publicId= $(this).parents('tr')[0].id;
            switch(control){
                case 'top':
                    if($(this).hasClass('active')) 
                        changePublicStatus(publicId,0,1);
                    else
                        changePublicStatus(publicId,0,2);
                    break;
                case 'view':
                    window.location.href = '';
                    break;
                case 'edit':
                    window.location.href = '';
                    break;
                case 'delete':
                    $(this).parents('tr').hide();
                    getAjax('/video/delete/'+publicId,'',function () {
                        $(this).parents('tr').hide();
                    })
                    break;
            }
        })

    })
    getAjax('/article/show/10/0','',function(callback){
        // console.log(callback)
        var articleHtml ='',
            data = callback.articles;
        for(var i = 0;i< data.length;i++){
            articleHtml += '<tr id="'+data[i].id+'">'
                +'<td scope="row">'+(i+1)+'</td>'
                +'<td><a href="">'+data[i].title+'</a></td>'
                +'<td>'+data[i].tag+'</td>'
                +'<td>管理员</td>'
                +'<td class="timeText">'+FormatDate(data[i].createTime,0)+'</td>'
                +'<td>'
                +    '<div class="btn-group publicControler2" data-toggle="buttons">'
                +      '<label class="btn btn-danger btn-xs publicOn">'
                +        '<input type="radio" name="options" autocomplete="off" > &nbsp;&nbsp;&nbsp;发布中&nbsp;&nbsp;&nbsp;'
                +      '</label>'
                +      '<label class="btn btn-danger btn-xs active ">'
                +        '<input type="radio" name="options" autocomplete="off" checked> &nbsp;&nbsp;&nbsp;下架中&nbsp;&nbsp;&nbsp;'
                +      '</label>'
                +    '</div>'
                +'</td>'
                +'<td>'
                +    '<button type="button" class="btn2 btn btn-default btn-xs" data-toggle="button" aria-pressed="false" autocomplete="off"><span class="glyphicon glyphicon-star"> </span>  推荐 </button>'
                +    '<button type="button" class="btn2 btn btn-default btn-xs" data-action="view"><span class="glyphicon glyphicon-eye-open"> </span> 查看 </button>'
                +    '<button type="button" class="btn2 btn btn-default btn-xs" data-action="edit"><span class="glyphicon glyphicon-edit"> </span> 编辑 </button>'
                +    '<button type="button" class="btn2 btn btn-default btn-xs" data-action="delete"><span class="glyphicon glyphicon-remove"> </span> 删除 </button>'
                +'</td>'
              +'</tr>'
        }
        $('#collapseTwo tbody').html(articleHtml);
        $('.publicControler2 label').click(function(){
            var publicId = $(this).parents('tr')[0].id;
            if($(this).hasClass('publicOn'))
                changePublicStatus(publicId,1,1);
            else if($(this).hasClass('publicOff'))
                changePublicStatus(publicId,1,0);
            // $(this).parents('tr').hide()
        })
        $('.btn2').click(function(){
            var control = $(this).data('action'),
                publicId= $(this).parents('tr')[0].id;
            switch(control){
                case 'top':
                    if($(this).hasClass('active')) 
                        changePublicStatus(publicId,1,1);
                    else
                        changePublicStatus(publicId,1,2);
                    break;
                case 'view':
                    // window.location.href = '/video/adminplay/'+publicId;
                    break;
                case 'edit':
                    window.location.href = '/kqmoocadmin/editarticle/'+publicId;
                    break;
                case 'delete':
                    $(this).parents('tr').hide();
                    getAjax('/article/delete/'+publicId,'',function () {
                        $(this).parents('tr').hide();
                    })
                    break;
            }
        })
    })
    
})
