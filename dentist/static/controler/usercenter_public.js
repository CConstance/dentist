$(function(){
    getAjax('/user/get/'+_userId,'',function(callback){
        // $('#user_avas').attr('src','/static/img/'+callback.avatorUrl);
        $('#user_name').html(callback.nickName);
        $('#user_about').html('关于: '+callback.nickName)
        $('#user_id').html('医生ID：'+(callback.docId==null||callback.docId==''?'暂无':callback.docId));
        $('#user_about_content').html(callback.description)
    })
    getAjax('/article/userget/'+_userId,'',function(callback){
        // console.log(callback)
        var data   = callback.articles,
            length = data.length,
            ulHtml = '',
            date   = '';
        if(!length){
            $('#articleList').html('<font style="font-size:23px;font-weight:400;color:#DFE3EA">(暂无发表)');
            return
        }
        for(var i = 0;i < length;i++){
            date = FormatDate(data[i].createTime,1)
            ulHtml += 
            '<li>'
            +  '<p class="left">'
            +    '<span class="date">'+date.day+'<u>'+date.month+'</u></span>'
            +    '<a href="/article/'+data[i].id+'" target="_blank" class="thumb"><img src="./static/uploads/article/poster/'+data[i].poster+'" alt="'+data[i].title+'"></a>'
            +  '</p>'
            +  '<div class="article_wrap">'
            +    '<div class="row">'
            +        '<div class="col-md-12">'
            +           '<p class="right" >'
            +               '<span class="title"><a href="/article/'+data[i].id+'" target="_blank">'+data[i].title+'</a></span>'
                            // '<span class="rel">7个月前<u>•</u>围观热度 4476<u>•</u><a href="/article/experience/">经验之谈</a></span>'
            +               '<span class="summary">'+data[i].description+'</span>'
            +               '<span class="tags">'
            +               '<span class="glyphicon glyphicon-tag"></span>'
            +                   '<a href="" target="_blank">'+data[i].tag+'</a>'
            +               '</span>'
            +           '</p>'
            +        '</div>'
            +     '</div>'
            +  '</div>'
            +'</li> '
        }
        
        $('#articleList').html(ulHtml);
        $('#articleList img').error(function () {
            $(this).attr('src',"/static/img/default3.jpg");
        })  
    })
})