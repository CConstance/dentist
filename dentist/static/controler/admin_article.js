$(function(){
    getContent(1);

    function getContent(pages){
        // console.log(pages)
        getAjax('/article/listall/'+type,{"pages":pages},function(callback){
        console.log(callback)
        var length = callback.articles.length,
            data   = callback.articles,
            html   = '',
            pageHtml = '<span class="disabled prev">«</span>',
            check  = '',
            activeControler = '',
            totalPage = callback.totalPage;
        for(var j = 1 ;j<=totalPage+1;j++){
            if(pages == j)
                pageHtml += '<span class="current " id="page-'+j+'">'+j+'</span>'
            else
                pageHtml += '<span class="default" id="page-'+j+'">'+j+'</span>'
        }
        for(var i = 0; i<length;i++){
            if(!data[i].status){
                check = 
                      '<label class="btn btn-danger btn-xs publicOn">'
                +        '<input type="radio" id="1" autocomplete="off" > &nbsp;&nbsp;&nbsp;发布中&nbsp;&nbsp;&nbsp;'
                +      '</label>'
                +      '<label class="btn btn-danger btn-xs active publicOff" >'
                +        '<input type="radio" id="0" autocomplete="off" > &nbsp;&nbsp;&nbsp;下架中&nbsp;&nbsp;&nbsp;'
                +      '</label>';
            }
            else{                
                check = 
                     '<label class="btn btn-danger btn-xs active publicOn">'
                +        '<input type="radio" id="1" autocomplete="off" > &nbsp;&nbsp;&nbsp;发布中&nbsp;&nbsp;&nbsp;'
                +      '</label>'
                +      '<label class="btn btn-danger btn-xs publicOff" >'
                +        '<input type="radio" id="0" autocomplete="off" > &nbsp;&nbsp;&nbsp;下架中&nbsp;&nbsp;&nbsp;'
                +      '</label>';
            }
            activeControler = data[i].status==2?"active":"";

            html +='<tr id="'+data[i].id+'">'
            +'<th scope="row">'+(i+1)+'</th>'
            +'<td><a href="'+data[i].id+'">'+data[i].title+'</a></td>'
            +'<td>'+data[i].tag+'</td>'
            +'<td>'+data[i].userId+'</td>'
            +'<td>'+FormatDate(data[i].createTime,0)+'</td>'
            +'<td>'
            +    '<div class="btn-group publicControler" data-toggle="buttons">'
            +check
            +    '</div>'
            +'</td>'
            +'<td>'
            +    '<button type="button" class="btn btn-default btn-xs '+activeControler+'" data-action="top" data-toggle="button" aria-pressed="false" autocomplete="off"><span class="glyphicon glyphicon-star"> </span>  置顶 </button>'
            +    '<button type="button" class="btn btn-default btn-xs" data-action="view"><span class="glyphicon glyphicon-eye-open"> </span> 查看 </button></a>'
            +    '<button type="button" class="btn btn-default btn-xs" data-action="edit"><span class="glyphicon glyphicon-edit"> </span> 编辑 </button>'
            +    '<button type="button" class="btn btn-default btn-xs" data-action="delete"><span class="glyphicon glyphicon-remove"> </span> 删除 </button>'
            +'</td>'
          +'</tr>'
        }
        pageHtml += '<span class="disabled next" >»</span>';
        $('tbody').html(html);
        $('.qzPagination').html(pageHtml)
        $('button').click(function() {
            var action = $(this).data('action'),
                publicId= $(this).parents('tr')[0].id;
            switch(action){
                case 'view':
                    // window.location.href = '/video/adminplay/'+publicId;
                    break;
                case 'edit':
                    window.location.href = '/kqmoocadmin/editarticle/'+publicId;
                    break;
                case 'top':
                    if($(this).hasClass('active')) 
                        changePublicStatus(publicId,1,1);
                    else
                        changePublicStatus(publicId,1,2);
                    break;
                case 'delete':
                    $(this).parents('tr').hide();
                    getAjax('/article/delete/'+publicId,'',function () {
                        $(this).parents('tr').hide();
                    })
                    break;
            }
        });
        $('.publicControler label').click(function(){
            var publicId = $(this).parents('tr')[0].id;
            if($(this).hasClass('publicOn'))
                changePublicStatus(publicId,1,1);
            else if($(this).hasClass('publicOff'))
                changePublicStatus(publicId,1,0);
            // $(this).parents('tr').hide()
        })
        $('.qzPagination span').click(function(){
            var spanId = this.id,
                pageId = Number(spanId.substr(5,spanId.length));
            getContent(pageId)
        })
        // window.location.href = callback.url 
    })
    }
    
})