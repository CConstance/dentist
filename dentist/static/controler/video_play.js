function getContent(id){
    getAjax('/video/get/'+id,'',function(callback){
        $('video').attr('src','/static/uploads/video/'+callback.videoUrl).attr('poster','/static/uploads/video/poster/'+callback.posterUrl);
        $('source').attr('src','/static/uploads/video/'+callback.videoUrl);
        $('.words h1').html(callback.videoName);
        $('.words p').html(callback.description);
    })  
}

function showTips(){
    $('video').attr('poster','/static/img/nuAuth.png')
}
