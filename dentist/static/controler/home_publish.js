$(function(){
    getAjax('/article/show/5/2','',function(callback){
        // console.log(callback)
        var articleHtml ='',
            data      = callback.articles,
            length    = data.length,
            innerHtml = '',
            carouselHtml = '';
        if(!length){
            // articleHtml = '<tr><td colspan="6">'+table_text_null+'</td></tr>';
            // $('#collapseOne tbody').html(articleHtml);
            return 
        }
        for(var i = 0;i< length;i++){
            if(i==0){
                carouselHtml += '<li data-target="#carousel-example-generic" data-slide-to="'+i+'" class="active"></li>'
                innerHtml    += '<div class="item active">'
                              +'<img src="/static/uploads/article/picture/'+data[i].picture+'">'
                              +'<div class="carousel-caption">'
                              + ' <h1>【 '+data[i].title+' 】</h1>'
                              +  '<p>'+(i+1)+'</p>'
                              +'</div>'
                            +'</div>'
            }
            else{
                carouselHtml += '<li data-target="#carousel-example-generic" data-slide-to="'+i+'"></li>'
                innerHtml    += 
                            '<div class="item">'
                              +'<img src="/static/uploads/article/picture/'+data[i].picture+'">'
                              +'<div class="carousel-caption">'
                              + ' <h1>【 '+data[i].title+' 】</h1>'
                              +  '<p>'+(i+1)+'</p>'
                              +'</div>'
                            +'</div>'
            }
                
        }
        $('.carousel-indicators').html(carouselHtml);
        $('.carousel-inner').html(innerHtml);
        $('.carousel-inner img').error(function(){
            $(this).attr('src',"/static/img/banner.jpg");
        })
    })

    
    getAjax('/video/getall',{"pages":1},function(callback){
        if(callback.videos.length)
            changeContent(callback.videos);
    })

    function changeContent(data){
        // console.log(data)
        var itemsHtml = '';
        for(var i = 0;i<data.length;i++){
            itemsHtml += 
                '<li>'
                +   ' <a href="/video/play/'+data[i].id+'" target="_blank" class="homeshade">'
                +        '<div class="shade "><u></u></div>'
                +        '<img src="/static/uploads/video/poster/'+data[i].postUrl+'" class="img-responsive">'
                +        '<p>'+data[i].tag+'<br>'
                +        '<strong>'+data[i].videoName+'</strong><br>'
                +        FormatDate(data[i].uploadTime,0)+'</p>'
                +        '<div class="bg"><u></u></div>'
                +    '</a>'
                +'</li>'
        }
        itemsHtml +='<a href="/video" title="" class="aMore" target="_blank">MORE</a>'
        $('.items').html(itemsHtml);
        $('.list-inline img').error(function(){
            $(this).attr('src',"/static/img/default2.jpg");
        })
    }
})