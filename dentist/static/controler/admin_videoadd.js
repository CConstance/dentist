$(function(){
    getAjax('/column/getColumnByType/0','',function(callback){
        var length = callback.columns.length,
            data   = callback.columns,
            html   = '';
        for(var i = 0;i<length;i++){
            html += '<option id="'+data[i].id+'">'+data[i].tag+'</option>'
        }
        $('.classify').html(html);
    })

    $("#file-1").fileinput({
        uploadUrl: '/video/uploadposter', 
        allowedFileExtensions : ['jpg','png','jpeg','gif'],
        overwriteInitial: false,
        // maxFileSize: 1000,
        // maxFilesNum: 10,
        allowedFileTypes: ['image'],
        slugCallback: function(filename) {
          console.log('ok')
          $('#postUrl').val(filename)
          return filename.replace('(', '_').replace(']', '_');
        }
    });
    $("#file-2").fileinput({
        uploadUrl: '/video/uploadvideo', 
        allowedFileExtensions : ['mp4'],
        overwriteInitial: false,
        // maxFileSize: 1000,
        // maxFilesNum: 10,
        allowedFileTypes: ['video'],
        slugCallback: function(filename) {
          console.log(filename)
          $('#videoUrl').val(filename)
          return filename.replace('(', '_').replace(']', '_');
        }
    });
    $('.commitBtn').click(function(){
        var title       = $('#title').val(),
            description = $('#describe').val(),
            // videoUrl    = $('#videoUrl').val(),
            // postUrl     = $('#postUrl').val(),
            postUrl     = $('._poster .file-caption-name').html(),
            videoUrl    = $('._video .file-caption-name').html(),
            // file-caption-name
            tag         = $("select option:checked").attr("id");
            console.log()
            getAjax('/video/add',{"videoName":title,"description":description,"videoUrl":videoUrl,"postUrl":postUrl,"tag":tag},function(){
                window.location.href = '/kqmoocadmin/video'
                // window.location.href = callback.url 
            })
    })
})