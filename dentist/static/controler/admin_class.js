$(function(){
    getAjax('/column/getall','',function(callback){
        console.log(callback.columns)
        var length = callback.columns.length,
            data   = callback.columns,
            count1 = 1,
            count2 = 1,
            count3 = 1,
            html1   = '',
            html2   = '',
            html3   = '',
            htmlNull = '<tr><td colspan="3">'+table_text_null+'</td></tr>';
        for(var i = 0;i<length;i++){
            console.log(data[i].type)
            switch(data[i].type){
                case 0:
                html1 += '<tr id="'+data[i].id+'">'
                    +    '<td>'+(count1++)+'</td>'
                    +    '<td>'+data[i].tag+'</td>'
                    // +    '<td>'+data[i].count+'</td>'
                    +    '<td><button type="button" class="btn btn-default btn-xs" data-action="delete"><span class="glyphicon glyphicon-remove"> </span> 删除 </button></td>'
                    +'</tr>'
                    break;
                case 1:
                html2 += '<tr id="'+data[i].id+'">'
                    +    '<td>'+(count2++)+'</td>'
                    +    '<td>'+data[i].tag+'</td>'
                    // +    '<td>'+data[i].count+'</td>'
                    +    '<td><button type="button" class="btn btn-default btn-xs" data-action="delete"><span class="glyphicon glyphicon-remove"> </span> 删除 </button></td>'
                    +'</tr>'
                    break;
                case 2:
                html3 += '<tr id="'+data[i].id+'">'
                    +    '<td>'+(count3++)+'</td>'
                    +    '<td>'+data[i].tag+'</td>'
                    // +    '<td>'+data[i].count+'</td>'
                    +    '<td><button type="button" class="btn btn-default btn-xs" data-action="delete"><span class="glyphicon glyphicon-remove"> </span> 删除 </button></td>'
                    +'</tr>'
                    break;
                default :
                    break; 
            }
        }
        if(html1=='')
            html1 = htmlNull
        if(html2=='')
            html2 = htmlNull
        if(html3=='')
            html3 = htmlNull
        $('#collapseOne tbody').html(html1);
        $('#collapseTwo tbody').html(html2);
        $('#collapseThree tbody').html(html3);

        $('.btn-xs').click(function(event) {
            // console.log(event)
            var action = $(this).data('action'),
                id= $(this).parents('tr')[0].id;
            switch(action){
                case 'delete':
                    $(this).parents('tr').hide();
                    getAjax('/column/delete/'+id,'',function () {
                        $(this).parents('tr').hide();
                    })
                    break;
                default:
                    break;
            }
        });    
    })
    $('.addClass').click(function(){
        var className = $('#newClass').val(),
            type      = $('.classify').val();
        getAjax('/column/add',{"className":className,"type":type},function(callback){
            window.location.href='/kqmoocadmin/class'
        })
    })
})