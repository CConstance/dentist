$(function () {
    $('#content').wangEditor();
    var type = '';
    getAjax('/article/get/'+id,'',function(data){
        $('#title').val(data.title);
        $('#describe').val(data.description);
        // console.log(data)
        $('.wangEditor-textarea').html(data.content);
        type = data.type
        if(type=='1'){
            $('.bg').css('top','128px').show()
        }       
        else
            $('.bg').css('top','160px').show()
        getColumn(data.tag)
    })
    function getColumn(tag){
        getAjax('/column/getColumnByType/'+type,'',function(callback){
            var length = callback.columns.length,
                data   = callback.columns,
                html   = '';
            for(var i = 0;i<length;i++){
                console.log(tag)
                if(tag==data[i].id)
                    html += '<option id="'+data[i].id+'" selected = selected>'+data[i].tag+'</option>'
                else
                    html += '<option id="'+data[i].id+'" >'+data[i].tag+'</option>'

            }
            $('.classify').html(html);
        })
    }
    $('.commitBtn').click(function(){
        var title   = $('#title').val(),
            description = $('#describe').val(),
            tag         = $("select option:checked").attr("id"),
            content     = $('#content').val();

        getAjax('/article/update/'+id,{"title":title,"description":description,"tag":tag,"content":content},function (calback) {
            if(type=='1')
                window.location.href= '/kqmoocadmin/article'
            else
                window.location.href='/kqmoocadmin/author'
        })
    })

})