$(function(){
    getAjax('/user/get/'+_userId,'',function(callback){
        // $('#user_avas').attr('src','/static/img/'+callback.avatorUrl);
        $('#user_name').html(callback.nickName);
        $('#user_about').html('关于: '+callback.nickName)
        $('#user_id').html('医生ID：'+(callback.docId==null||callback.docId==''?'暂无':callback.docId));
        $('#user_about_content').html(callback.description)
    })
    getAjax('/column/getall','',function(callback){
        var length = callback.columns.length,
            data   = callback.columns,
            html   = '';
        for(var i = 0;i<length;i++){
            html += '<option id="'+data[i].id+'">'+data[i].tag+'</option>'
        }
        $('.classify').html(html);
    })
    // var editor =  $('#content').wangEditor()
    $('#content').wangEditor();
    $("#file-1").fileinput({
        uploadUrl: '/article/uploadposter', 
        allowedFileExtensions : ['jpg','png','jpeg','gif'],
        overwriteInitial: false,
        // maxFileSize: 1000,
        // maxFilesNum: 10,
        allowedFileTypes: ['image'],
        slugCallback: function(filename) {
          console.log('ok')
          $('#postUrl').val(filename)
          return filename.replace('(', '_').replace(']', '_');
      }
    })

    $('.commitBtn').click(function () {
        var title   = $('#title').val(),
        description = $('#describe').val(),
        tag         = $("select option:checked").attr("id"),
        content     = $('#content').val(),
        postUrl     = $('#_poster .file-caption-name').html();
        // picUrl      = $('#_pic .file-caption-name').html();
        getAjax('/article/add',{"title":title,"content":content,"description":description,"postUrl":postUrl,"tag":tag,"picUrl":''},function(){
            window.location.href = '/usercenter/public'
                // window.location.href = callback.url 
            })

    })
});