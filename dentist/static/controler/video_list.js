$(function () {
    getAjax('/column/getColumnByType/0','',function(callback){
        var length = callback.columns.length,
            data   = callback.columns,
            categoryHtml = '<a href="/video" class="btn btn-default btn-primary">全部</a>'
            dropdownHtml = '<li><a href="/video">全部</a></li>';
        for(var i = 0;i<length;i++){
            categoryHtml += '<a class="btn btn-default" id="'+data[i].id+'">'+data[i].tag+'</a>'
            dropdownHtml += '<li><a id="'+data[i].id+'">'+data[i].tag+'</a></li>'
        }
        $('.tag_normal').html(categoryHtml);
        $('.tag_drop').html(dropdownHtml);
        $('.tag_normal .btn').click(function(){
            $(this).siblings().removeClass('btn-primary');
            $(this).addClass('btn-primary');
            getByTag(this.id)
        })
    })

    getAjax('/video/getall',{"pages":1},function(callback){
        if(callback.videos.length)
            changeContent(callback.videos);
    })

    function getByTag(tagId){
        getAjax('/video/list/10/'+tagId,'',function(callback){
            console.log(callback)
            changeContent(callback.videos);
        })
    }

    function changeContent(data){
        // console.log(data)
        var itemsHtml = '';
        for(var i = 0;i<data.length;i++){
            itemsHtml += 
                '<li>'
                +   ' <a href="/video/play/'+data[i].id+'" target="_blank">'
                +        '<div class="shade"><u></u></div>'
                +        '<img src="/static/uploads/video/poster/'+data[i].postUrl+'" class="img-responsive">'
                +        '<p>'+data[i].tag+'<br>'
                +        '<strong>'+data[i].videoName+'</strong><br>'
                +        FormatDate(data[i].uploadTime,0)+'</p>'
                +        '<div class="bg"><u></u></div>'
                +    '</a>'
                +'</li>'
        }
        $('.items').html(itemsHtml);
        $('.list-inline img').error(function(){
            $(this).attr('src',"/static/img/default2.jpg");
        })
    }
    
})