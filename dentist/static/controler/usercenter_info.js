$(function(){
    // 获取资料
    var formValue ={};
    var target = '';
    formValue['id'] = _userId;
    getAjax('/user/get/'+_userId,'',function(callback){
        // $('#user_avas').attr('src','/static/img/'+callback.avatorUrl);
        $('#user_name').html(callback.nickName);
        $('#user_about').html('关于: '+callback.nickName)
        $('#user_id').html('医生ID：'+(callback.docId==null||callback.docId==''?'暂无':callback.docId));
        $('#name').val(callback.name);
        $('#nickName').val(callback.nickName);
        $('#docId').val(callback.docId);
        $('#address').val(callback.address);
        $('#telephone').val(callback.telephone);
        $('#description').val(callback.description);
        $('#user_about_content').html(callback.description)
        saveVal();
        $('.form-group').keyup(function(event){
            target = $(event.target)
            checkVal(target);
        })
    })
    $('.save').click(function(event) {
        saveVal();
        getAjax('/user/update',formValue,function(callback){
            // console.log(callback)
            location.reload()
        })
    });
    function checkVal(target){
        if(!input_Validate(target.val(),'s',0)){
            target.next().hide();
            changeCheck();
        }
        else{
            $('.save').addClass('disabled')
            target.next().html('请输入正确字符');
            target.next().show();
        }
    }
    function saveVal(){
        $("input[type='text']").each(function(){
            // console.log($(this).val())
            formValue[this.id] = $(this).val()
            // formValue.push({'this.id' : $(this).val()})
        })
        
    }
    // 输入监听
    function changeCheck(){
        // var
        var singal = 0
        $("input[type='text']").each(function(){
            if($(this).val()!=formValue[this.id]){
                $('.save').removeClass('disabled')
                singal =1
            }
        })
        if(!singal){
            $('.save').addClass('disabled') 
        }
            

    }
    // 提交资料 
})