$(function(){
    getAjax('/video/show/10/2','',function(callback){
        // console.log(callback)
        var videoHtml ='',
            data      = callback.videos,
            length    = data.length;
        if(!length){
            videoHtml = '<tr><td colspan="6">'+table_text_null+'</td></tr>';
            $('#collapseTwo tbody').html(videoHtml);
            return 
        }
        for(var i = 0;i< length;i++){
            videoHtml += '<tr id="'+data[i].id+'">'
                +'<td scope="row">'+(i+1)+'</td>'
                +'<td><a href="">'+data[i].videoName+'</a></td>'
                +'<td>'+data[i].tag+'</td>'
                +'<td>管理员</td>'
                +'<td class="timeText">'+FormatDate(data[i].uploadTime,0)+'</td>'
                +'<td>'
                +    '<button type="button" class="videoTop btn btn-danger btn-xs"  aria-pressed="false" autocomplete="off"><span class="glyphicon glyphicon-remove"> </span>  取消置顶 </button>'
                +'</td>'
              +'</tr>'
        }
        $('#collapseTwo tbody').html(videoHtml);
        $('.videoTop').click(function(){
            var publicId = $(this).parents('tr')[0].id;
            changePublicStatus(publicId,0,1);
            $(this).parents('tr').hide()
        })
    })
    
    getAjax('/article/show/10/2','',function(callback){
        // console.log(callback)
        var articleHtml ='',
            data      = callback.articles,
            length    = data.length;
        if(!length){
            articleHtml = '<tr><td colspan="6">'+table_text_null+'</td></tr>';
            $('#collapseOne tbody').html(articleHtml);
            return 
        }
        for(var i = 0;i< length;i++){
            articleHtml += '<tr id="'+data[i].id+'">'
                +'<td scope="row">'+(i+1)+'</td>'
                +'<td><a href="">'+data[i].title+'</a></td>'
                +'<td>'+data[i].tag+'</td>'
                +'<td>管理员</td>'
                +'<td class="timeText">'+FormatDate(data[i].createTime,0)+'</td>'
                +'<td>'
                +    '<button type="button" class="articleTop btn btn-danger btn-xs"  aria-pressed="false" autocomplete="off"><span class="glyphicon glyphicon-remove"> </span>  取消置顶 </button>'
                +'</td>'
              +'</tr>'
        }
        $('#collapseOne tbody').html(articleHtml);
        $('.articleTop').click(function(){
            var publicId = $(this).parents('tr')[0].id;
            changePublicStatus(publicId,1,1);
            $(this).parents('tr').hide()
        })
    })
})
