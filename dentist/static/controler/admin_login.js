$(function(){
    $('.account').blur(function(){
        var accountState = input_Validate($('.account').val(),'e',0);
        if(!accountState) $('#account').hide();
        if(accountState==2){
            $('#account').html('请输入正确邮箱地址');
            $('#account').show();
        }
    })
    $('.password').blur(function(){
        var passState = input_Validate($('.password').val(),'p','6');
        if(!passState) $('#password').hide();
        else if(passState==1){
            $('#password').html('密码至少为6为哦');
            $('#password').show();
        }
        else if(passState==2){
            $('#password').html('密码仅有数字和字母组成');
            $('#password').show();
        }
    })
    $('.login').click(function(){
        var account      = $('.account').val(),
            password     = $('.password').val(),
            accountState = input_Validate(account,'e',0),
            passState    = input_Validate(password,'p','6');
        if(!accountState&&!passState){
            password  = $.md5(password);
            getAjax('/user/login',{"account":account,"password":password},function(callback){
                if(callback.role == '1')
                    window.location.href = '/kqmoocadmin';
                else{
                    $('#password').html('请使用管理员账号登录');
                    $('#password').show();
                }

                // window.location.href = callback.url 
            })
        }
            
        else{
            if(accountState==2){
                $('#account').html('请输入正确邮箱地址');
                $('#account').show();
            }
            if(passState==1){
                $('#password').html('密码至少为6为哦');
                $('#password').show();
            }
            else if(passState==2){
                $('#password').html('密码仅有数字和字母组成');
                $('#password').show();
            }
        }
    })
})