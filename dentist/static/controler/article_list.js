$(function () {
    // FormatDate (strTime,type)
    if(type=='1')
        $('.bg').css('top','64px')
    else
        $('.bg').css('top','96px')
    $('.bg').show()
    getAjax('/article/listall/'+type,{'pages':1},function(callback){
        var data   = callback.articles,
            length = data.length,
            ulHtml = '',
            date   = '';
        if(!length){
            $('#articleList').html();
            return
        }
        for(var i = 0;i < length;i++){
            date = FormatDate(data[i].createTime,1)
            ulHtml += 
            '<li>'
            +  '<p class="left">'
            +    '<span class="date">'+date.day+'<u>'+date.month+'</u></span>'
            +    '<a href="/article/'+data[i].id+'" target="_blank" class="thumb"><img src="./static/uploads/article/poster/'+data[i].poster+'" alt="'+data[i].title+'"></a>'
            +  '</p>'
            +  '<div class="article_wrap">'
            +    '<div class="row">'
            +        '<div class="col-md-12">'
            +           '<p class="right" >'
            +               '<span class="title"><a href="/article/'+data[i].id+'" target="_blank">'+data[i].title+'</a></span>'
                            // '<span class="rel">7个月前<u>•</u>围观热度 4476<u>•</u><a href="/article/experience/">经验之谈</a></span>'
            +               '<span class="summary">'+data[i].description+'</span>'
            +               '<span class="tags">'
            +               '<span class="glyphicon glyphicon-tag"></span>'
            +                   '<a href="" target="_blank">'+data[i].tag+'</a>'
            +               '</span>'
            +           '</p>'
            +        '</div>'
            +     '</div>'
            +  '</div>'
            +'</li> '
        }
        $('#articleList').html(ulHtml);
        $('#articleList img').error(function () {
            $(this).attr('src',"/static/img/default3.jpg");
        })
    })
})