$(function(){
    getContent(1);

    function getContent(pages){
        // console.log(pages)
        getAjax('/video/getall',{"pages":pages},function(callback){
        // console.log(callback)
        var length = callback.videos.length,
            data   = callback.videos,
            html   = '',
            pageHtml = '<span class="disabled prev">«</span>',
            check  = '',
            activeControler = '',
            totalPage = callback.total==0?0:callback.total;
        for(var j = 1 ;j<=totalPage+1;j++){
            if(pages == j)
                pageHtml += '<span class="current " id="page-'+j+'">'+j+'</span>'
            else
                pageHtml += '<span class="default" id="page-'+j+'">'+j+'</span>'
        }
        for(var i = 0; i<length;i++){
            if(!data[i].status){
                check = 
                      '<label class="btn btn-danger btn-xs publicOn">'
                +        '<input type="radio" id="1" autocomplete="off" > &nbsp;&nbsp;&nbsp;发布中&nbsp;&nbsp;&nbsp;'
                +      '</label>'
                +      '<label class="btn btn-danger btn-xs active publicOff" >'
                +        '<input type="radio" id="0" autocomplete="off" > &nbsp;&nbsp;&nbsp;下架中&nbsp;&nbsp;&nbsp;'
                +      '</label>';
            }
            else{                
                check = 
                     '<label class="btn btn-danger btn-xs active publicOn">'
                +        '<input type="radio" id="1" autocomplete="off" > &nbsp;&nbsp;&nbsp;发布中&nbsp;&nbsp;&nbsp;'
                +      '</label>'
                +      '<label class="btn btn-danger btn-xs publicOff" >'
                +        '<input type="radio" id="0" autocomplete="off" > &nbsp;&nbsp;&nbsp;下架中&nbsp;&nbsp;&nbsp;'
                +      '</label>';
            }
            activeControler = data[i].status==2?"active":"";

            html +='<tr id="'+data[i].id+'">'
            +'<th scope="row">'+(i+1)+'</th>'
            +'<td><a href="/video/adminplay/'+data[i].id+'">'+data[i].videoName+'</a></td>'
            +'<td>'+data[i].tag+'</td>'
            +'<td>管理员</td>'
            +'<td>'+FormatDate(data[i].uploadTime,0)+'</td>'
            +'<td>'
            +    '<div class="btn-group publicControler" data-toggle="buttons">'
            +check
            +    '</div>'
            +'</td>'
            +'<td>'
            +    '<button type="button" class="btn btn-default btn-xs '+activeControler+'" data-action="top" data-toggle="button" aria-pressed="false" autocomplete="off"><span class="glyphicon glyphicon-star"> </span>  置顶 </button>'
            +    '<button type="button" class="btn btn-default btn-xs" data-action="view"><span class="glyphicon glyphicon-eye-open"> </span> 查看 </button></a>'
            +    '<button type="button" class="btn btn-default btn-xs" data-action="edit"><span class="glyphicon glyphicon-edit"> </span> 编辑 </button>'
            +    '<button type="button" class="btn btn-default btn-xs" data-action="delete"><span class="glyphicon glyphicon-remove"> </span> 删除 </button>'
            +'</td>'
          +'</tr>'
        }
        pageHtml += '<span class="disabled next" >»</span>';
        $('tbody').html(html);
        $('.qzPagination').html(pageHtml)
        $('button').click(function() {
            var action = $(this).data('action'),
                publicId= $(this).parents('tr')[0].id;
            switch(action){
                case 'view':
                    window.location.href = '/video/adminplay/'+publicId;
                    break;
                case 'edit':
                    window.location.href = '/kqmoocadmin/videoedit/'+publicId;
                    break;
                case 'top':
                    if($(this).hasClass('active')) 
                        changePublicStatus(publicId,0,1);
                    else
                        changePublicStatus(publicId,0,2);
                    break;
                case 'delete':
                    $(this).parents('tr').hide();
                    getAjax('/video/delete/'+publicId,'',function () {
                        $(this).parents('tr').hide();
                    })
                    break;
            }
        });
        $('.publicControler label').click(function(){
            var publicId = $(this).parents('tr')[0].id;
            if($(this).hasClass('publicOn'))
                changePublicStatus(publicId,0,1);
            else if($(this).hasClass('publicOff'))
                changePublicStatus(publicId,0,0);
            // $(this).parents('tr').hide()
        })
        $('.qzPagination span').click(function(){
            var spanId = this.id,
                pageId = Number(spanId.substr(5,spanId.length));
            getContent(pageId)
        })
        // window.location.href = callback.url 
    })
    }
    
})