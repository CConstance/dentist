function  input_Validate(str,type,length) {
    // 0：正确; 1: 长度不正确；2：输入不正确
    var notice = '';
    if(str.length<length){
        return 1;
    }

    var reg = '';
    switch(type){
        case 'n':
            reg = /^([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)$/;
            break;
        case 'e':
            reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
            break;
        case 's':
            reg = /^[\u4E00-\u9FA5\uf900-\ufa2d\w\.\s]+$/
            break;
        case 'p':
            reg = /^[0-9a-zA-Z]*$/
            break;
    }
    if(str.match(reg)==null)
        return 2
    else return 0
}

// var csrftoken = $('meta[name=csrf-token]').attr('content')
// $.ajaxSetup({
//     beforeSend: function(xhr, settings) {
//         if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type)) {
//             xhr.setRequestHeader("X-CSRFToken", csrftoken)
//         }
//     }
// })

function getAjax(url, data, callback) {
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',      
        data: JSON.stringify(data),
        success: callback
    });
}



function getQuery(name){
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]);
    return null;
}

function FormatDate (strTime,type) {
    var date = new Date(strTime);
    if(!type)
        return date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
    else{
        var monthArray = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"),
            month      = date.getMonth(),
            day        = date.getDate(),
            newDate    ={};
            // if(day.toString().length == 1){
            //     day="0"+day.toString();
            // }
            newDate.month = monthArray[month];
            newDate.day   = day
            return newDate
        }
}

function changePublicStatus(publicId,type,status){
    getAjax('/status/update/'+type+'/'+publicId+'/'+status,'','')
}

var table_text_null = '(暂时没有条目哦)';

function getUserStatus(role){
    var user=['普通用户','管理员','专家'];
    return user[role]
}
